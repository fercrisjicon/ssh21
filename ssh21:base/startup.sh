#! /bin/bash

useradd -m -s /bin/bash unix01
useradd -m -s /bin/bash unix02
useradd -m -s /bin/bash unix03
echo -e "unix01\nunix01" | passwd unix01
echo -e "unix02\nunix02" | passwd unix02
echo -e "unix03\nunix03" | passwd unix03

cp /opt/docker/login.defs /etc/login.defs
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
cp /opt/docker/pam_mount.conf.xml /etc/security/pam_mount.conf.xml
cp /opt/docker/common-* /etc/pam.d/.
cp /opt/docker/ns* /etc/.

/usr/sbin/nscd
/etc/init.d/nslcd start
mkdir /run/sshd
/usr/sbin/sshd -D
