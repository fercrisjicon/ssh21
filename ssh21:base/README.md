# SSH Server
## @edt ASIX M06-ASO 2021-2022
### Servidor SSH (Debian 11)

Podeu trobar les imatges docker al Dockehub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Podeu trobar la documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona


 * **fercrisjicon/ssh21:base** Servidor SSH base inicial amb la configuracio de client i servidor per defecte

```
#entrar dins del servidor SSH:
docker run --rm --name ssh.edt.org -h ssh.edt.org --net hisix2 -it cristiancondolo21/ssh21:base /bin/bash

#per practicar, engegar servidor per detach:
docker run --rm --name ssh.ed.org -h ssh.edt.org --net 2hisix -d cristiancondolo21/ssh21:base
#tenir el host del server grabat en la nostra maquina:
/etc/hosts
[IP_server] 	ssh.edt.org

#per practicar, entra com unix01 amb ssh:
ssh unix01@ssh.edt.org
```
```
#Per engegar totes les maquines en detach per la practica de docker-compose
docker-compose up -d
```
